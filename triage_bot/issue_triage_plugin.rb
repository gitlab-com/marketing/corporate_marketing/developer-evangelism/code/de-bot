require 'json'
require 'date'
require "faraday"
require 'dotenv/load'

module DATriagePlugin
  def last_comment_at
    conn = Faraday.new(
      url: notes_url+"?sort=desc&order_by=created_at&pagination=keyset&per_page=1",
      headers: {'PRIVATE-TOKEN' => ENV.fetch("PRIV_KEY"), 'Content-Type' => 'application/json' }
    )
   
    response = conn.get()
    if response.status == 200
      jsonData = JSON.parse(response.body)
      if jsonData.length > 0
        Date.parse(jsonData[0]['created_at'])
      else
        Date.parse(resource[:created_at])
      end
    else
      Date.parse(resource[:created_at])
    end
  end

  def notes_url
    resource[:_links][:notes]
  end

  def get_project_id
    resource[:project_id]
  end

  def get_current_quarter_last_date()
    yr = Time.now.year
    case Time.now.month
    when 2..4
      lm = 4
    when 5..7
      lm = 7
    when 8..10
      lm = 10
    when 11..12
      lm = 1
      yr = yr + 1
    else
      lm = 1    
    end
    
    return Date.new(yr, lm, -1) 
  end
  


  def one_week_to_due_date
    if(resource[:due_date] == nil)
      false
    else
      days_to_due = (Date.parse(resource[:due_date]) - Date.today).to_i
      if(days_to_due > 0 && days_to_due < 7)
        true
      else
        false
      end
    end
  end


  def due_date_past
    if(resource[:due_date] == nil)
      false
    else
      Date.today > Date.parse(resource[:due_date])
    end
  end

  def missing_due_date
    if(resource[:due_date] == nil)
      true
    else
      false
    end
  end


end

Gitlab::Triage::Resource::Context.include DATriagePlugin
