# GitLab Developer Advocacy Bot

A collection of automation tools for the GitLab Developer Advocacy team to manage issues, generate reports, and handle content workflows.

## 🚀 Features

- Automated issue management and labeling
- Weekly content and triage reports generation
- Release evangelism coordination
- Due date management
- Call for Papers (CFP) tracking
- Content workflow automation
- Team request classification

## 📋 Documentation

- [Bot Documentation](docs/bot.md) - Details about the DevRel Bot implementation and features
- [Triage Documentation](docs/triage.md) - Comprehensive guide to the GitLab Triage policies and plugin

## 🔧 Setup

### Requirements

- Ruby 3.3+
- Required gems:
  - gitlab
  - json
  - jsonpath
  - date
  - dotenv
  - faraday
  - gitlab-triage

### Environment Variables

Create a `.env` file with the following variables:
```bash
GITLAB_COM_ID=<gitlab-group-id>
DEFAULT_PROJECT_ID=<project-id> # This is the project where Reports and other issues are created, i.e. da-tmm-meta project.
PRIV_KEY=<gitlab-api-token>
GITLAB_WEBSITE_PROJECT=<website-project-id>
DEBUG=0  # Set to 1 for debug mode
```

## 🏃‍♂️ Running the Applications

### DevRel Bot
```bash
# Normal execution
ruby bot.rb

# Debug mode
DEBUG=1 ruby bot.rb
```

### Triage Bot
```bash
# Normal execution
gitlab-triage -r ./triage_bot/issue_triage_plugin.rb --token $PRIV_KEY --source-id gitlab-com --source groups

# Debug/Dry run
gitlab-triage -r ./triage_bot/issue_triage_plugin.rb --dry-run --debug --token $PRIV_KEY --source-id gitlab-com --source groups
```

## 🔄 CI/CD Pipeline

The project includes automated CI/CD pipelines running on GitLab CI:

### Stages
1. `test` - Dependency scanning and secret detection
2. `dryrun` - Test runs with debug mode
3. `execute` - Production execution

### Jobs
- `run_bot` - Executes the DevRel bot
- `test_bot` - Tests the DevRel bot in debug mode
- `test-issue-triage` - Manual trigger for testing triage rules
- `issue-triage` - Executes triage rules in production

### Schedule
The pipeline runs automatically on the master branch:
- Schedule: Monday-Friday at 12:00 UTC
- Cron: `0 12 * * 1-5`

### Security
Includes GitLab's security scanning templates:
- Dependency scanning
- Secret detection

## 📝 Contributing

1. Create an issue to discuss proposed changes
2. Fork the repository
3. Create a feature branch
4. Add and test your changes
5. Submit a merge request

## ⚠️ Important Notes

- Always use `--dry-run` when testing triage rules
- Set `DEBUG=1` for testing the DevRel bot
- Keep environment variables secure
- Review the documentation before making changes


## 📜 License

MIT License
Copyright (c) GitLab B.V.

## 🤝 Support

For support, please create an issue in the project repository or contact the Developer Advocacy team. You can also reference other documentation or Handbook pages as follows:
- [Project Handbook](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/#developer-evangelism-bot)
- [Developer Evangelism Workflows](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/workflow/)