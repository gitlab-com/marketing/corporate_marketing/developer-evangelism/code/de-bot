require 'dotenv'
require 'gitlab'
Dir['./lib/*.rb'].each {|file| require file }

Dotenv.load


bot = Bot.new()

puts "***********************"

puts "Welcome to the GitLab DevRel Bot"

puts "Running in DEBUG mode" if ENV['DEBUG']

bot.issueReports()

puts "***********************"

