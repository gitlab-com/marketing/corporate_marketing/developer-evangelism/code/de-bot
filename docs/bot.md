# GitLab DevRel Bot Documentation

## Overview
The GitLab DevRel Bot is an automation tool designed to assist the Developer Advocacy team at GitLab. It handles various tasks including issue tracking, report generation, and release evangelism coordination.

## System Requirements
- Ruby
- Required gems:
  - gitlab
  - json
  - jsonpath
  - date
  - dotenv
  - uri

## Main Components

### 1. Bot Class
**Location**: `lib/bot.rb`

The main orchestrator class that initializes the application and coordinates between different components.

#### Methods

##### `initialize()`
Creates a new Bot instance and initializes the GitLab API client.

##### `releaseEvangelism()`
Handles the release evangelism process by:
- Runs on the 18th of each month
- Fetches details about the next release
- Creates evangelism issues for team members
- Comments on merge requests to notify the team
- Returns a status message

##### `issueReports()`
Manages the generation of team reports:
- Runs on Fridays at 12:00 PM UTC
- Generates both triage and content reports
- Creates issues with the reports in GitLab

##### `triageIssueReport()` and `contentIssueReport()`
Helper methods that:
- Create formatted reports using the IssueReport class
- Post the reports as new issues in GitLab
- Add appropriate labels and assignees

### 2. GitLabAPI Class
**Location**: `lib/gitlabapi.rb`

Handles all interactions with the GitLab API.

#### Methods

##### `initialize(api_url, privToken)`
Creates a new GitLab API client instance with the provided credentials.

##### `createIssue(project_id, assignee, description, title, labels)`
Creates a new issue in GitLab with the specified parameters.
- Automatically adds the "DA-Bot::Auto" label
- Returns the issue ID
- Includes debug output when in DEBUG mode

##### `commentOnIssue(project_id, issue_iid, comment)`
Adds a comment to an existing issue.

##### `commentOnMR(project_id, mr_iid, comment)`
Adds a comment to a merge request.

##### `determineNextMilestone(due_date)`
Calculates the next milestone date:
- Takes a due date and calculates the next Friday
- Returns the milestone ID matching the format "Fri: Jun 5, 2020"

##### `getIssues(resource_id, extraArgs)`
Retrieves issues from a GitLab group with pagination support.

##### `getNextRelease()`
Fetches information about the next release:
- Searches for open merge requests with specific labels
- Returns release version, link, and IID

### 3. IssueReport Class
**Location**: `issues_report.rb`

Handles the generation and formatting of various issue reports.

#### Methods

##### `initialize(gitlab_api)`
Creates a new IssueReport instance with the provided GitLab API client.

##### `getCurrentQuarter()`
Determines the current quarter based on the current date:
- Q1: February - April
- Q2: May - July
- Q3: August - October
- Q4: November - January

##### `issuesCount(label, state = 1)`
Counts issues with specific labels and states within the current quarter.

##### `issues_opened_last_week(label)`
Retrieves issues opened in the last 7 days with the specified label.

##### `issues_closed_last_week(label)`
Retrieves issues closed in the last 7 days with the specified label.

##### `create_issues_table(issues, table_type = 'open')`
Formats issue data into markdown tables with different layouts based on the table type:
- 'open': Shows title, author, assignees, and creation date
- 'closed': Shows title, author, assignees, and closing date
- 'triage': Shows title, author, assignees, labels, creation date, and update date

##### `weekly_issues_report_triage()` and `weekly_issues_report_content()`
Generate comprehensive weekly reports:
- Include links to relevant handbook pages and issue boards
- Show different issue categories and their current status
- Format data in markdown tables
- Add appropriate labels and team mentions

## Environment Variables
The application requires several environment variables:
- `GITLAB_COM_ID`: GitLab group ID
- `DEFAULT_PROJECT_ID`: Default project ID for creating issues
- `PRIV_KEY`: GitLab API private token, the `gitlab_devrel_bot` user's token is used here.
- `DEBUG`: Enable debug mode
- `GITLAB_WEBSITE_PROJECT`: Project ID for the GitLab website
- `RELEASE_EVANGELISM_DATA`: JSON data for release evangelism team members

## Usage
1. Set up required environment variables
2. Run the main script: `ruby bot.rb`

The bot will:
- Generate release evangelism issues on the 18th of each month
- Create weekly reports on Fridays at 12:00 PM UTC
- Add appropriate labels and mentions to created issues

## Debug Mode
When `DEBUG=true`:
- Shows detailed output instead of creating actual issues
- Prints formatted content that would be posted
- Allows testing without affecting the GitLab instance
- Debug data is generated as an artifact when the bot jobs run.

## Labels
The bot uses several label categories:
- `DA-Bot::Auto`: Automatically added to all bot-created issues
- `DA-Status::FYI`: For informational issues
- `DA-Weekly-Reports`: For weekly report issues
- `developer-advocacy`: Team-specific label
- Various content status labels (New, In-Progress, In-Review, etc.)