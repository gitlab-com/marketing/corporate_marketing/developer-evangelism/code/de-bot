# GitLab Triage Technical Documentation

# Policy Rules

## Team Member Policies

1. **DevRel-Influenced Issues**
   - **Name**: Issues where DA team member is an assignee outside DA-Meta project
   - **Purpose**: Track influence across GitLab projects
   - **Conditions**:
     - Assignee is member of DA team (group 10087220)
     - Issue is open
     - Not in DA-Meta project (18266508)
     - No developer-advocacy label
   - **Actions**: Adds labels
     - developer-advocacy
     - DevRel-Influenced
     - DA-Bot::Skip

2. **Team Member Issue Labeling**
   - **Name**: Issue by DA team member missing developer-advocacy label
   - **Purpose**: Ensure proper team labeling
   - **Conditions**:
     - Author is DA team member
     - Issue is open
     - No developer-advocacy label
   - **Actions**: Adds developer-advocacy label

## Type Labeling Policies

3. **DA-Type Label Check**
   - **Name**: Issue missing DA-Type label
   - **Purpose**: Ensure issue categorization
   - **Conditions**: Missing all type labels (Consulting, Events, Content, etc.)
   - **Actions**: Adds triage labels for review

4. **Content Type Check**
   - **Name**: Content Issue missing DA-Type-Content label
   - **Purpose**: Ensure content categorization
   - **Conditions**: Missing specific content type labels
   - **Actions**: Adds triage labels for content type review

## Content Workflow Policies

5. **New Content Status**
   - **Purpose**: Track new content
   - **Conditions**: Has DA-Content::New label
   - **Actions**: Sets ToDo status

6. **In-Progress Content**
   - **Purpose**: Track active content
   - **Conditions**: Has DA-Content::In-Progress label
   - **Actions**: Sets Doing status

7. **Metrics Pending Status**
   - **Purpose**: Track content awaiting metrics
   - **Conditions**: Has DA-Content::Done-Metrics-Pending label
   - **Actions**: Sets OnHold status

8. **Publication Status**
   - **Purpose**: Track content awaiting publication
   - **Conditions**: Has DA-Content::Awaiting-Publication label
   - **Actions**: Sets OnHold status

9. **Content Completion**
   - **Purpose**: Handle published content
   - **Conditions**: Content marked as published
   - **Actions**: 
     - Closes issue
     - Sets Done status
     - Adds completion comment

## Due Date Management Policies

10. **Missing Due Dates**
    - **Purpose**: Ensure proper scheduling
    - **Conditions**: No due date set
    - **Actions**: 
      - Sets due date to quarter end
      - Adds auto-due-date label

11. **On-Hold Review**
    - **Purpose**: Prevent stale on-hold issues
    - **Conditions**: 
      - On hold for 90+ days
      - No recent comments
    - **Actions**: Adds triage labels and reminder comment

12. **Past Due Date Handling**
    - **Purpose**: Track overdue issues
    - **Conditions**: Due date passed
    - **Actions**: 
      - Adds triage labels
      - Updates bot-set due dates

## Activity Monitoring Policies

13. **Inactive Issues**
    - **Purpose**: Track stale issues
    - **Conditions**: No updates for 60+ days
    - **Actions**: 
      - Adds triage labels
      - Posts reminder comment

14. **Completed but Open**
    - **Purpose**: Ensure proper closure
    - **Conditions**: 
      - Marked as Done
      - Still open
    - **Actions**: 
      - Adds triage labels
      - Requests metrics documentation

## Request Classification Policies

15. **Internal Requests**
    - **Purpose**: Track team requests
    - **Conditions**: Author is team member
    - **Actions**: Adds internal requester label

16. **External Requests**
    - **Purpose**: Track external requests
    - **Conditions**: Author not team member
    - **Actions**: Adds external requester label

## CFP Management Policies

17. **CFP Due Dates**
    - **Purpose**: Track CFP deadlines
    - **Conditions**: CFP issue missing due date
    - **Actions**: Adds triage labels

18. **CFP Stage Tracking**
    - **Purpose**: Monitor CFP progress
    - **Conditions**: Past due date for various stages
    - **Actions**: Adds stage-specific triage labels

## Consulting Request Policies

19. **Team Assignment**
    - **Purpose**: Ensure proper routing
    - **Conditions**: Missing consulting team label
    - **Actions**: Adds triage labels for team assignment

## Cleanup Policies

20. **Auto-close Bot Issues**
    - **Purpose**: Maintain issue hygiene
    - **Conditions**: 
      - Bot-created issue
      - Older than 2 weeks
      - No skip label
    - **Actions**: 
      - Closes issue
      - Adds explanation comment

## GitLab Tiage Plugin
The GitLab Triage Plugin is a Ruby-based extension for the gitlab-triage gem that adds custom functionality for Developer Advocacy team automation. It provides methods for due date management, comment tracking, and project identification.

## Setup and Execution

### Requirements
- Ruby
- gitlab-triage gem
- Required gems:
  - json
  - date
  - faraday
  - dotenv

### Running the Triage Script
```bash
gitlab-triage -r ./triage_bot/issue_triage_plugin.rb --dry-run --debug --token $PRIV_KEY --source-id gitlab-com --source groups
```

#### Command Line Arguments
- `-r`: Path to the plugin Ruby file
- `--dry-run`: Test mode without making actual changes
- `--debug`: Enable detailed logging
- `--token`: GitLab API token
- `--source-id`: GitLab group identifier
- `--source`: Resource type (groups)

## Plugin Implementation

### Module: DATriagePlugin
The plugin is implemented as a Ruby module that extends Gitlab::Triage::Resource::Context.

#### Methods

##### `last_comment_at`
```ruby
def last_comment_at
```
- Fetches the most recent comment date for an issue
- Uses Faraday for HTTP requests
- Returns:
  - Date of the last comment
  - Creation date if no comments exist
- Error handling:
  - Falls back to issue creation date on API failures

##### `notes_url`
```ruby
def notes_url
```
- Returns the API URL for issue notes/comments
- Accessed via resource links

##### `get_project_id`
```ruby
def get_project_id
```
- Returns the project ID for the current resource

##### `get_current_quarter_last_date`
```ruby
def get_current_quarter_last_date
```
- Calculates the last date of the current quarter
- Quarter definitions:
  - Q1: February - April
  - Q2: May - July
  - Q3: August - October
  - Q4: November - January
- Returns a Date object

##### `one_week_to_due_date`
```ruby
def one_week_to_due_date
```
- Checks if the due date is within the next week
- Returns:
  - `true`: if due date is 1-7 days away
  - `false`: if no due date or outside range

##### `due_date_past`
```ruby
def due_date_past
```
- Checks if the due date has passed
- Returns:
  - `true`: if due date is in the past
  - `false`: if no due date or in future

##### `missing_due_date`
```ruby
def missing_due_date
```
- Checks if a due date is set
- Returns:
  - `true`: if no due date
  - `false`: if due date exists

## Integration with Triage Rules

### Rule Conditions
The plugin methods can be used in rule conditions:

```yaml
conditions:
  ruby: due_date_past && Date.today - last_comment_at > 90
```

### Ruby Expressions
Supported expressions:
- `get_project_id != PROJECT_ID`
- `missing_due_date`
- `due_date_past`
- `Date.today - last_comment_at > N`

### Dynamic Values
The plugin provides dynamic values for:
- Due dates
- Comment timestamps
- Project identification
- Quarter calculations

## HTTP Integration

### Faraday Configuration
```ruby
conn = Faraday.new(
  url: notes_url+"?sort=desc&order_by=created_at&pagination=keyset&per_page=1",
  headers: {
    'PRIVATE-TOKEN' => ENV.fetch("PRIV_KEY"),
    'Content-Type' => 'application/json'
  }
)
```

### API Endpoints
- Notes API: Used for comment retrieval
- Pagination: Keyset-based with single item limit
- Authentication: Uses private token from environment

## Environment Variables
Required environment variables:
- `PRIV_KEY`: GitLab API private token for the `gitlab_devrel_bot` user.

## Error Handling
The plugin includes error handling for:
- API request failures
- Missing due dates
- Invalid date formats
- Missing comments

## Best Practices

### Plugin Development
1. **Error Handling**
   - Always provide fallback values
   - Log API failures appropriately
   - Use defensive programming for dates

2. **Date Calculations**
   - Use consistent date formats
   - Handle edge cases (month transitions)
   - Consider timezone implications

3. **API Integration**
   - Minimize API calls
   - Use pagination effectively
   - Handle rate limiting

### Rule Implementation
1. **Condition Construction**
   - Combine conditions logically
   - Use plugin methods efficiently
   - Consider performance impact

2. **Action Design**
   - Keep actions atomic
   - Provide clear feedback
   - Allow manual override

## Testing
To test the plugin:
1. Use `--dry-run` flag
2. Enable debug mode
3. Verify condition evaluations
4. Check API responses
5. Validate date calculations



# Troubleshooting
Common issues and solutions:
1. **API Authentication**
   - Verify PRIV_KEY environment variable
   - Check token permissions
   - Validate API access

2. **Date Calculations**
   - Verify quarter boundaries
   - Check date format consistency
   - Validate timezone handling

3. **Rule Execution**
   - Review condition syntax
   - Check label existence
   - Verify project access