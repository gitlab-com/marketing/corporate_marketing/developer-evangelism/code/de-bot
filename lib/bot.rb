require 'json'
require 'jsonpath'
require 'date'
require 'uri'
require 'gitlab'

class Bot

    def initialize()
        @date_now = DateTime.now
        @gitLabAPI = GitLabAPI.new('https://gitlab.com/api/v4/', ENV['PRIV_KEY'])
    end

    def releaseEvangelism()
       
        # Generates Release Evangelism issue
       
        data = " ============================== "
        data += "Release Evangelism: \r\n"
        data += " ------------------------------ "
        dueDate = @date_now.strftime('%Y').to_s
        dueDate.concat('-')
        dueDate.concat(@date_now.strftime('%m').to_s)
        dueDate.concat('-22')
        if @date_now.strftime('%d').to_s == '18'
            mr_details = @gitLabAPI.getNextRelease()
            #puts mr_details.to_yaml
            evangelism_data = JSON.parse(ENV['RELEASE_EVANGELISM_DATA']);
            evangelism_data.each do |team_member|
                issue_content = File.read('issue_templates/release_evangelism.txt')
                issue_content['FIXME_MR'] = mr_details[:link].to_s
                release_version = mr_details[:release_version].to_s
                
                issue_content['FIXME_REVIEWAPP'] = 'https://release-'+release_version.gsub('.','-').to_s+'.about.gitlab-review.app/blog/'
                data = data + @gitLabAPI.createIssue(ENV['DEFAULT_PROJECT_ID'].to_s, team_member['id'].to_s, issue_content , team_member['name'].concat(" Release evangelism: "+release_version), 'dev-evangelism,DE-Bot::Auto,DE-Type::Evangelist,DE-Status::ToDo', dueDate).to_s
                
            end
            
            begin
                @gitLabAPI.commentOnMR(ENV['GITLAB_WEBSITE_PROJECT'], mr_details[:iid].to_s, "FYI @gitlab-de for "+mr_details[:release_version].to_s+" release evangelism.")
            rescue StandardError => e  
                e.message +" :: "+ e.backtrace.inspect  
            end 
            
        else
         data += "Next Release issue will be created  on 18th of the month"
        end

        data
    end

    def issueReports()

        # Generates for Content and Triage reports for the Developer Advocacy Team

        puts " ============================== "
        puts "Reports: \r\n"
        puts" ------------------------------ "
        if (@date_now.strftime('%u').to_s == '5' && @date_now.strftime('%H').to_s == '12') || ENV['DEBUG']
            triageIssueReport()
            contentIssueReport()
        else
            puts "Next Report will be created on Friday at 12:00 PM UTC"
        end
        #puts data
    end

    def triageIssueReport()
        tday = DateTime.now
        il = IssueReport.new(@gitLabAPI)
        @gitLabAPI.createIssue(ENV['DEFAULT_PROJECT_ID'].to_s, ENV['ABUBAKAR_GITLAB_ID'].to_s, il.weekly_issues_report_triage, 'Developer Advocacy Issue Triage Report: '+ tday.strftime("%Y-%m-%d").to_s, 'developer-advocacy,DA-Bot,DA-Type::Process')
        #il.weekly_issues_report_triage
        puts "Triage Report created"
    end

    def contentIssueReport()
        tday = DateTime.now
        il = IssueReport.new(@gitLabAPI)
        @gitLabAPI.createIssue(ENV['DEFAULT_PROJECT_ID'].to_s, ENV['ABUBAKAR_GITLAB_ID'].to_s, il.weekly_issues_report_content, 'Developer Advocacy Content Report: '+ tday.strftime("%Y-%m-%d").to_s, 'developer-advocacy,DA-Bot,DA-Type::Process')
        #il.weekly_issues_report_content
        puts "Content Report created"
    end


end
