require 'json'
require 'jsonpath'
require 'date'
require 'gitlab'
require './lib/gitlabapi.rb'

class IssueReport

  def initialize(gitlab_api)
    @gitLabAPI = gitlab_api
    @date_now = DateTime.now
  end

  def getCurrentQuarter()
    month = @date_now.strftime('%m')

    if month >= 2 && month <= 4
      q = {:q => 1, :start_month => 2}
    elsif month >= 5 && month <= 7
      q = {:q => 2, :start_month => 5}
    elsif month >= 8 && month <= 10
      q = {:q => 3, :start_month => 8}
    else
      q = {:q => 4, :start_month => 11}
    end

    q
  end

  def issuesCount(label, state = 1)

      current_quarter = getCurrentQuarter()  

      current_quarter_start_year = @date_now.strftime('%m') == 1 ? @date_now.strftime('%Y') - 1 : @date_now.strftime('%Y') 

      first_date_quarter = Date.parse(current_quarter_start_year.to_s+'-'+current_quarter['start_month'].to_s+'-1')

      if state == 1
        response = @gitLabAPI.getIssues(ENV['GITLAB_COM_ID'], {state: 'opened', labels: label, created_after: first_date_quarter.rfc3339})
        
      else
        response = @gitLabAPI.getIssues(ENV['GITLAB_COM_ID'], {state: 'closed', labels: label, updated_after: first_date_quarter.rfc3339})
        
      end
   

    reponse.count

  end


  # Function to get issues opened in the last 7 days
  def issues_opened_last_week(label)
    one_week_ago = (Date.today - 7).rfc3339
    response = @gitLabAPI.getIssues(ENV['GITLAB_COM_ID'], {state: 'opened', labels: label, created_after: one_week_ago})
    response
  end

 def issues_closed_last_week(label)
   one_week_ago = Date.today - 7
   response = @gitLabAPI.getIssues(ENV['GITLAB_COM_ID'], {
     state: 'closed', 
     labels: label, 
     updated_after: one_week_ago.rfc3339
   })
   
   # Filter issues that were closed within the last week
   response.select do |issue|
     closed_at = issue['closed_at'] ? Date.parse(issue['closed_at']) : nil
     closed_at && closed_at >= one_week_ago
   end
 end

  #Function to get issues with the label DA-Bot::Triage	
  def issues_with_label(label)
    response = @gitLabAPI.getIssues(ENV['GITLAB_COM_ID'], {state: 'opened', labels: label})
    response
  end

  def create_issues_table(issues, table_type = 'open')
    table_row = ""
    if table_type == 'triage'
      table_row += "| Title | Author |  Assignees | Labels | Created | Updated |"
      table_row += "\n|  --- |  --- | --- | --- | --- | --- |"
    elsif table_type == 'open'
      table_row += "| Title | Author |  Assignees | Created |"
      table_row += "\n|  --- |  --- | --- | --- |"
    else
      table_row+= "| Title | Author |  Assignees |  Closed |"
      table_row += "\n|  --- |  --- | --- | --- | "
    end
   

    issues.each do |issue|
      assignees = []
      issue['assignees'].each { |assignee| assignees.append(assignee['username']) }

      table_row += "\n | [#{issue['title']}](#{issue['web_url']})  | `#{issue['author']['username']}`  | `#{assignees.join("`, `")}` "
      
      if table_type == 'triage'
        table_row += "| ~\"#{issue['labels'].join('" ~"')}\" | #{Date.parse(issue['created_at'])} | #{Date.parse(issue['updated_at'])} |"
      elsif table_type == 'closed'
        table_row += "| #{Date.parse(issue['closed_at'])} |"
      else
         table_row += " | #{Date.parse(issue['created_at'])} | "
      end
      
    end
    table_row
  end
  

  def weekly_issues_report_triage

    #get issues opened in the last 7 days
    opened_issues = create_issues_table(issues_opened_last_week('developer-advocacy'), 'open')
    closed_issues = create_issues_table(issues_closed_last_week('developer-advocacy'), 'closed')
    triage_issues = create_issues_table(issues_with_label('DA-Bot::Triage'), 'triage' )

    issue_desc = <<-EOS
# Developer Advocacy Issues Triage - Weekly Report

- [Team Workflow Hanbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/workflow/)
- **Issue Boards**
  - [Issues by Assignee](https://gitlab.com/groups/gitlab-com/-/boards/7577841?label_name[]=developer-advocacy) 
  - [Content by Quarter](https://gitlab.com/groups/gitlab-com/-/boards/7577857?label_name[]=DA-Type%3A%3AContent&label_name[]=developer-advocacy) 
  - [Content by Types](https://gitlab.com/groups/gitlab-com/-/boards/7577822?label_name[]=DA-Type%3A%3AContent&label_name[]=developer-advocacy) 
  - [Events](https://gitlab.com/groups/gitlab-com/-/boards/7577874?label_name[]=developer-advocacy&label_name[]=DA-Type%3A%3AEvents) 
  - [Issue Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/7669248?label_name[]=DA-Bot%3A%3ATriage) 

## Issues opened in the last 7 days
      
#{opened_issues}

## Issues closed in the last 7 days

#{closed_issues}

## Issues with the label DA-Bot::Triage

#{triage_issues}  




/cc @gitlab-da
/label ~"DA-Status::FYI" ~"DA-Weekly-Reports" ~"developer-advocacy"

EOS

issue_desc
  end

  def weekly_issues_report_content
    #get issues opened in the last 7 days
    new_issues = create_issues_table(issues_with_label('DA-Content::New'), 'open')
    inprogress = create_issues_table(issues_with_label('DA-Content::In-Progress'), 'open')
    inreview = create_issues_table(issues_with_label('DA-Content::In-Review'), 'open')
    done_metrics_pending = create_issues_table(issues_with_label('DA-Content::Done-Metrics-Pending'), 'open')
    awaiting_publication = create_issues_table(issues_with_label('DA-Content::Awaiting-Publication'), 'open')
    published = create_issues_table(issues_with_label('DA-Content::Published'), 'closed')

    issue_desc = <<-EOS
# Developer Advocacy Weekly Content Report

- [Team Workflow Hanbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/workflow/)
- **Issue Boards**
  - [Issues by Assignee](https://gitlab.com/groups/gitlab-com/-/boards/7577841?label_name[]=developer-advocacy) 
  - [Content by Quarter](https://gitlab.com/groups/gitlab-com/-/boards/7577857?label_name[]=DA-Type%3A%3AContent&label_name[]=developer-advocacy) 
  - [Content by Types](https://gitlab.com/groups/gitlab-com/-/boards/7577822?label_name[]=DA-Type%3A%3AContent&label_name[]=developer-advocacy) 
  - [Events](https://gitlab.com/groups/gitlab-com/-/boards/7577874?label_name[]=developer-advocacy&label_name[]=DA-Type%3A%3AEvents) 
  - [Issue Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/7669248?label_name[]=DA-Bot%3A%3ATriage) 

      
## New Content Issues
      
#{new_issues}

## Content in Progress

#{inprogress}

## Content in Review

#{inreview}

## Awaiting Publication

#{awaiting_publication}

## Done with Metrics Pending

#{done_metrics_pending}

## Published & Closed

#{published}

/cc @gitlab-da
/label ~"DA-Status::FYI" ~"DA-Weekly-Reports" ~"developer-advocacy"

EOS

issue_desc

end

end
