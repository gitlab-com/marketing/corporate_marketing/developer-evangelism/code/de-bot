require 'json'
require 'jsonpath'
require 'date'
require 'uri'
require 'gitlab'

class GitLabAPI

  def initialize(api_url, privToken)
    @GitLabClient =  Gitlab.client(endpoint: api_url, private_token: privToken)
  end


  def createIssue(project_id, assignee, description, title, labels )
    labels += ",DA-Bot::Auto"

    begin
      if ENV['DEBUG']
debug_output = <<-EOS
================================
Creating Issue
================================
Project ID: #{project_id}
Assignee ID: #{assignee}
Title: #{title}
Labels: #{labels}
Description: 

#{description}
================================
EOS
puts debug_output
      else
        issue = @GitLabClient.create_issue(project_id, title, { description: description, assignee_id: assignee, labels: labels})
        issue.id
      end
    rescue StandardError => e  
       e.message +" :: "+ e.backtrace.inspect  
    end  

  end

  def commentOnIssue(project_id, issue_iid, comment)
      begin
        if ENV['DEBUG'] == 1
debug_output = <<-EOS
================================
Commenting on Issue
================================
Issue IID: #{issue_iid}
Project ID ID: #{project_id}
Comment: 

#{comment}
================================
EOS
puts debug_output
      else
          @GitLabClient.create_issue_note(project_id, issue_iid, body)
        end
      rescue StandardError => e  
        puts e.message  
        puts e.backtrace.inspect  
      end  
  end

  def commentOnMR(project_id, mr_iid, comment)
    begin
      if ENV['DEBUG'] == 1
debug_output = <<-EOS
================================
Commenting on MR
================================
MR IID: #{mr_iid}
Project ID ID: #{project_id}
Comment: 

#{comment}
================================
EOS
puts debug_output
      else
        mr = @GitlabClient.create_merge_request_discussion(project_id, mr_iid, body: comment.to_s)
      end
    rescue StandardError => e  
       e.message +" :: "+ e.backtrace.inspect  
    end  
  end

  def determineNextMilestone(due_date)

    #Determine next friday
    #Milestone Format: Fri: Jun 5, 2020
    #due_date
    dueDate = Date.parse(due_date)
    dueDateWeekDay = dueDate.cwday.to_i
    dayDiff = 0
      if dueDateWeekDay < 5
        dayDiff =  5 - dueDateWeekDay
      elsif dueDateWeekDay == 5
             dayDiff = 0
      else
        dayDiff = (7 - dueDateWeekDay) + 5
      end

    dueDate += dayDiff
    milestone_text = dueDate.strftime("%a: %b %-d, %Y").to_s
    begin
      data = @GitLabClient.group_milestones(ENV['GITLAB_COM_ID'], {state: 'active', search: milestone_text})
      data.first['id']
    rescue StandardError => e  
      e.message +" :: "+ e.backtrace.inspect  
    end  

  end

  def getIssues( resource_id, extraArgs)
    begin
      group_issues = @GitLabClient.group_issues(resource_id.to_s, extraArgs)
      group_issues.auto_paginate
    rescue StandardError => e  
      e.message+" :: "+ e.backtrace.inspect  
    end  

  end

  def getNextRelease()
    begin
      merge_requests = @GitLabClient.merge_requests(ENV['GITLAB_WEBSITE_PROJECT'], :state => 'opened', :labels => 'release, release post', :search => "Release post kickoff ", :in => 'description', :order_by => 'created_at')
      {:link => merge_requests.first['web_url'], :release_version => merge_requests.first['milestone']['title'], :iid => merge_requests.first['iid']}
    rescue StandardError => e  
      e.message+" :: "+ e.backtrace.inspect  
    end 
  end

end
